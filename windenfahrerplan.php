<?php
// Copyright 2018,2021,2023 Tobias Grundmann
//
// This file is part of Windenfahrerplan.
//
// Windenfahrerplan is free software: you can redistribute it and/or modify
// it under the terms of version 3 of the GNU General Public License as
// published by the Free Software Foundation
//
// Windenfahrerplan is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Windenfahrerplan. If not, see <http://www.gnu.org/licenses/>.
error_reporting(E_ALL);
error_log("[" .  $_SERVER['DOCUMENT_ROOT'] ."][" .  $_SERVER['SCRIPT_NAME'] . "]");

define('MAINDIR',dirname(__FILE__) . '/');
require_once ( MAINDIR . "/config.php");
require_once ( MAINDIR . "/plan.php");
require_once ( MAINDIR . "/weather.php");

function printPost() {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
                echo "<div id='post' style='border:solid;background-color:white;display:inline-block;float:right;padding: 0.5em'>";
                foreach ( $_POST as $key => $value ) {
                        printf("<p>POST [%s] => [%s]</p>", $key, $value);
                }
                echo "<p>[" .  $_SERVER['DOCUMENT_ROOT'] ."][" .  $_SERVER['SCRIPT_NAME'] . "]</p>";
                echo "</div>";
        }
}

function sanitize($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
}

$plan = Plan::laden();
if (WEATHER) {
        $weather = Weather::load();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
        foreach ( $_POST as $key => $value ) {
                error_log("POST [" . $key . "] => [" . $value . "]", 0);
        }
        if (isset($_POST["action"])) {
                $action = isset($_POST["action"]) ? sanitize($_POST["action"]) : null;
                $fahrerName = isset($_POST["fahrerName"]) ? sanitize($_POST["fahrerName"]) : null;
                $fahrerId = isset($_POST["fahrerId"]) ? sanitize($_POST["fahrerId"]) : null;
                $windenfahrerGS = isset($_POST["windenfahrerGS"]) ? sanitize($_POST["windenfahrerGS"]) : null;
                $windenfahrerHG = isset($_POST["windenfahrerHG"]) ? sanitize($_POST["windenfahrerHG"]) : null;
                $EWF = isset($_POST["EWF"]) ? sanitize($_POST["EWF"]) : null;
                $datum = isset($_POST["datum"]) ? sanitize($_POST["datum"]) : null;
                $spaet = isset($_POST["spaet"]) ? sanitize($_POST["spaet"]) : null;
                $frueh = isset($_POST["frueh"]) ? sanitize($_POST["frueh"]) : null;
                $hinweisText = isset($_POST["hinweisText"]) ? sanitize($_POST["hinweisText"]) : null;
                $resourceID = isset($_POST["resourceID"]) ? sanitize($_POST["resourceID"]) : null;
                $resourceValue = isset($_POST["resourceValue"]) ? sanitize($_POST["resourceValue"]) : null;
                $terminTitel = isset($_POST["terminTitel"]) ? sanitize($_POST["terminTitel"]) : null;
                $terminId = isset($_POST["terminId"]) ? sanitize($_POST["terminId"]) : null;
                
                if ($action == "neuerFahrer") {
                        $plan->neuerFahrer($fahrerName, $windenfahrerGS, $windenfahrerHG, $EWF);
                } else if ($action == "fahrerAendern") {
                        $plan->fahrerAendern($fahrerId, $fahrerName, $windenfahrerGS, $windenfahrerHG, $EWF);
                } else if ($action == "fahrerLoeschen") {
                        $plan->fahrerLoeschen($fahrerId);
                } else if ($action == "anmelden") {
                        $plan->update($fahrerId, $datum, $spaet, $frueh);
                } else if ($action == "hinweis") {
                        $plan->hinweis($hinweisText);
                } else if ($action == "staticResourceChange") {
                        $plan->staticResourceChanged($resourceID, $resourceValue);
                } else if ($action == "neuerTermin") {
                        $plan->neuerTermin($datum,$terminTitel);
                } else if ($action == "terminAendern") {
                        $plan->terminAendern($terminId,$terminTitel,$datum);
                } else if ($action == "terminLoeschen") {
                        $plan->terminLoeschen($terminId);
                }
        } else if (isset($_POST["Zufallswerte"])) {
                // $plan = new Plan();
                // $plan->random();
        }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="de">
<head>
<!-- Copyright 2018,2023 Tobias Grundmann -->
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1" />
<title><?php echo HTML_TITLE;?></title>
<link rel="stylesheet" type="text/css" href="css/styles.css" />
<!--
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400&display=swap" rel="stylesheet">
-->
<!--
        <link rel="icon" href="52N13E.svg" type="image/svg+xml">
        -->
</head>

<body>
	<div>
		<div class="headerDiv">
			<?php echo "<img src='" . LOGO ."' alt='Logo'/>"?>
		</div>
		<div class="headerDiv">
			<h1><?php echo HEADER;?></h1>
		</div>
		<div id="demo" class="headerDiv"><?php echo MOTTO;?></div>
	</div>
        <?php
        if (HINTS) {
                echo "<div><p>";
                echo "<button id='hinweisButton' class='button'>Aktuelle Hinweise</button> ";
                echo "<span id='hinweistext'>" . $plan->aktuelleHinweise . "</span>";
                if ($plan->aktuelleHinweise) {
                        echo "<span id='hinweistextaenderungszeit'><small> (" . $plan->getHinweisDateTimeFormatted() . ") </small></span>";
                }
                echo "</p></div>";
        }
        if (DEBUG) {
                printPost();
        }
        echo "<div>";
        if ($plan->error) {
                echo "<span class='error'>Error [" . $plan->errorMsg . "]</span>";
        }
        if (WEATHER && $weather->error != null) {
                echo "<span class='error'>Error retrieving weather information [" . $weather->fetchTime->format("c") . "][" . $weather->error . "]</span>";
        }
        echo "</div>";

        if (RESOURCE_STATUS && isset($plan->staticResourcesWithStatus) && count($plan->staticResourcesWithStatus) > 0) {

                echo "<div id='staticResourceContainer'>";
                echo "<p>";
                echo "<table class='staticResourceTable' id='staticResourceTable'>";

                echo "<colgroup class='resourceCol'>\n";
                foreach ( $plan->staticResourcesWithStatus as $key => $value ) {
                        echo "<col class='resourceCol'/>\n";
                }
                echo "</colgroup>\n";
                echo "<tr class='ampelzeile'>\n";
                $lights = array (
                                'red' => '-',
                                'yellow' => '~',
                                'green' => '+'
                );
                foreach ( $plan->staticResourcesWithStatus as $key => $value ) {
                        echo "<td class='resource' id='" . $key . "'>\n";
                        echo "<div class='ampel'>\n";
                        foreach ( $lights as $lightClass => $lightValue ) {
                                echo "<div class='light " . $lightClass . " " . ($value == $lightValue ? "light_on" : "light_off") . "'></div>\n";
                        }
                        /*
                         * echo "<div class='light red'></div>";
                         * echo "<div class='light yellow'></div>";
                         * echo "<div class='light green'></div>";
                         */
                        echo "</div>\n";
                        // echo $value;
                        echo "</td\n>";
                }
                echo "</tr>\n";
                echo "<tr class='staticResource'>\n";
                foreach ( $plan->staticResourcesWithStatus as $key => $value ) {
                        echo "<td class='resource'>";
                        echo $key;
                        echo "</td>";
                }
                echo "</tr>\n";

                echo "</table>";
                echo "</p>";
                echo "</div>";
        }
        if (!$plan->isFloating()) {
                echo "<p><button id='terminButton' class='button'>Neuer Termin</button></p>";
        }
        ?>

        <div id="tableContainer">

		<table id="bereitschaftTable" class="bereitschaftTable">
        <?php
        echo "<tr class='datumszeile'>";
        echo "<th style='width: 1.5em'>&nbsp;</th>";
        echo "<td style='width: 12em'></td>";
        // echo "<th>&nbsp;</th>";
        echo "<th class='fahrerFeature' style='width: 2.5em'>&nbsp;</th>";
        foreach ($plan->referenzBereitschaft->tage as $tag) {
                echo "<th class='tag'>";
                echo $tag->dm();
                echo "</th>";
        }
        echo "</tr>\n";
        if (!$plan->isFloating()) {
                echo "<tr class='terminzeile'>";
                echo "<th style='width: 1.5em'>&nbsp;</th>";
                echo "<td style='width: 12em' class='row_head'>Termine</td>";
                echo "<th class='fahrerFeature' style='width: 2.5em'>&nbsp;</th>";
                foreach ($plan->termine as $termin) {
                        echo "<th class='termin active' id='" . $termin->id . "'>";
                        echo $termin->render();
                        echo "</th>";
                }              
                echo "</tr>\n";
        }
        if (WEATHER && $weather->error == null) {
                echo "<tr class='wetterzeile'>";
                echo "<th style='width: 1.5em'>&nbsp;</th>";
                echo "<td style='width: 12em' class='row_head'><div>Wind </div><div>Böen &#13214;/h</div><div>Niederschlag &#13212;</div></td>";
                // echo "<th>&nbsp;</th>";
                echo "<th class='fahrerFeature' style='width: 2.5em'>&nbsp;</th>";
                foreach ($plan->referenzBereitschaft->tage as $tag) {
                        echo "<td class='weather_tag'>";
                        echo $weather->getEncodedIntervalsForDay($tag);
                        echo "</td>";
                }
                echo "</tr>\n";
        }
        $first = true;
        foreach ($plan->bereitschaften as $bereitschaft) {
                $fahrer = $bereitschaft->fahrer;
                echo "<tr class='fahrerzeile'>";
                if ($first) {
                        echo "<th class='arrow_cell' id='arrow_" . $fahrer->id . "'><span class='no_arrow_span'></span></th>";
                        $first = false;
                } else {
                        echo "<th class='arrow_cell' id='arrow_" . $fahrer->id . "'><span class='active arrow'>&uarr;</span></th>";
                }
                if (array_key_exists($fahrer->name,Plan::$specialParticipants)) {
                        echo "<th class='fahrer' id='" . $fahrer->id . "'>";                        
                } else {
                        echo "<th class='fahrer active' id='" . $fahrer->id . "'>";
                }
                // echo "<span class='active fahrerMenu'>&#x25BE;</span>";
                echo "<div class='fahrerDiv'>";
                if (DEBUG) {
                        echo '<span style="font-size:40%"> (' . $bereitschaft->score() . ')</span>';
                } 
                echo "<span>" . $fahrer->renderContent() . "</span>";
                echo "</div>";
                echo "</th>";
                echo "<td class='fahrerFeature'>" . $fahrer->renderFeature() . "</td>";
                foreach ($bereitschaft->tage as $tag ) {
                        echo "<td class='bereitschaft' id='$tag->id'>";
                        echo $tag->renderContent();
                        echo "</td>";
                }
                echo "</tr>\n";
        }
        ?>
        </table>
	</div>
	<div>
		<button id="neuerFahrerButton" class="button">Neuer Teilnehmer</button>
	</div>
        <?php
        // echo '<form method="post" action="' . $_SERVER['SCRIPT_NAME'] . '">';
        // echo ' <div id="zufallButton"><input type="submit" name="Zufallswerte" value="Zufallswerte" class="button"/></div>';
        // echo '</form>';
        ?>
        <div id="legende">
		<p />
		<hr />
		<i>Legende:</i>
		<p />
		<table id="legendeTable" style="overflow-x: auto;">
			<tr>
				<th>Fr&uuml;h</th>
				<th>Sp&auml;t</th>
				<th />
			</tr>
			<tr>
				<th colspan='2'>&#x2713;</th>
				<th>Ich bin dabei</th>
			</tr>
			<tr>
				<th colspan='2'>?</th>
				<th>Wei&szlig; nicht</th>
			</tr>
			<tr>
				<th colspan='2'>&times;</th>
				<th>Ich bin nicht da</th>
			</tr>
		</table>

		<p>
		<?php 
		if (WEATHER) {
			echo("Wetter: <a href='ttps://www.dwd.de/'>DWD</a>, ICON via <a href='https://open-meteo.com'>open-meteo.com</a> mit Lizenz: <a href='https://creativecommons.org/licenses/by/4.0/'>CC BY 4.0</a>");
		}
		?>
			<small>
				<!-- &#129045; --> <br /> Der Pfeil<span class='active'>&uarr;</span>blendet
				vorherige Zeilen aus (Eingabehilfe f&uuml;r kleine Bildschirme zur
				besseren Orientierung). <br /> Teilnehmer, die mehr als <?php echo Plan::$lastActionDelta; ?> Tage nicht aktiv waren, werden automatisch gel&ouml;scht und m&uuml;ssen sich neu anmelden.
                  <br /> Teilnehmer, die sich f&uuml;r einen
				fr&uuml;heren Tag eintragen, werden nach oben sortiert. <br /> Nach <?php $ds = new DateInterval(Plan::$dailySwitch); echo $ds->format('%H:%I h'); ?> bestimmen die Folgetage die Sortierreihenfolge.
                  <br /> &copy; Tobias Grundmann (GPL) <br /> <a
				href="http://www.steilspirale.net/schleppkalender/index.html">Schleppkalender
					Download</a> <br /> <span class="version"></span>
			</small>
		</p>
	</div>
	<hr />

	<div id="modal" class="modal">
		<div id="neuerFahrerPopup" class="overlayHidden">
			<div id="closeNeuerFahrer" class="close active">
				<span>&times;</span>
			</div>
			<br />
			 <form id="neuerFahrerForm" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']?>">
				<fieldset>
					<input type="hidden" name="action" value="neuerFahrer" /> 
					<input	type="hidden" name="fahrerId" value="" />
					<legend id="fahrerPopupH">Neuer Teilnehmer</legend>
					<b> <label>Name <input type="text" name="fahrerName" /></label><br />
						<label>WindenfahrerIn GS<input type="checkbox"
							name="windenfahrerGS" value="windenfahrerGS" /></label><br /> 
						<label>WindenfahrerIn HG<input type="checkbox" name="windenfahrerHG"
							value="windenfahrerHG" /> </label><br /> 
						<label>EWF <input type="checkbox" name="EWF" value="EWF" />
					</label><br />
					</b>
				</fieldset>
				<div>
					<input type="submit" value="abschicken" class="button" />
					<input id="fahrerLoeschen" type="submit" value="l&ouml;schen" class="button" />
				</div>
			</form>
		</div>

		<div id="terminPopup" class="overlayHidden">
			<div id="closeTermin" class="close active">
				<span>&times;</span>
			</div>
			<br />
			<form id="terminForm" method="post" action="<?php echo  $_SERVER['SCRIPT_NAME']?>"> 
				<fieldset>
					<input type="hidden" name="action" value="neuerTermin" />
					<input type="hidden" name="terminId" value="" />
					
						<b> 
						<label>Titel						
							<input type="text" name="terminTitel"
					    	   <?php 								 
					    	   echo 'maxlength="' . $plan->terminTitelMaxLen . '"'; 
					    	   echo 'size="' . $plan->terminTitelMaxLen . '"';
								?>             
								/>
						</label>
						<br /> 
						<label id = "terminDatumLabel" >Datum
					    	   <input type="<?php echo 'date'?>" name="datum"
					    	   <?php $ds = (new DateTime("today"))->format('Y-m-d');
					    	   echo " placeholder='". $ds . "'";
					    	   echo " min='". $ds . "'";
					    	   ?> 
					    	   pattern="\d{4}-\d{2}-\d{2}" required/>
					    </label>
						<br />
						</b>
				</fieldset>
				<div>
					<input type="submit" value="abschicken" class="button" />
					<input id="terminLoeschen" type="submit" value="l&ouml;schen" class="button" />
				</div>
			</form>
		</div>
		<div id="hinweisPopup" class="overlayHidden">
			<div id="closeHinweis" class="close active">
				<span>&times;</span>
			</div>
			<br />
			<form id="hinweisForm" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']?>">
				<fieldset>
					<input type="hidden" name="action" value="hinweis" />
					<legend id="hinweisePopupH">Max. <?php echo $plan->hinweisMaxLen ?> Zeichen. (<span
							id="charcount"></span>)
					</legend>

					<b> <!--<label>Text: <input type="text"     name="hinweisText"/></label><br/>-->
						<textarea cols="50" rows="5" name="hinweisText" id="hinweisText"
							maxlength="<?php echo $plan->hinweisMaxLen ?>"></textarea>
				
 				</b>
				</fieldset>
				<div>
					<input type="submit" value="abschicken" class="button" />
				</div>

			</form>
		</div>
		<div id="fahrerAnmeldungPopup" class="overlayHidden">
			<form id="anmeldeform" method="post" action="<?php echo $_SERVER['SCRIPT_NAME']?>">
				<fieldset>
					<input type="hidden" name="fahrerId" value="" /> 
					<input type="hidden" name="datum" value="" />
					<input type="hidden" name="action" value="anmelden" />
					<input type="hidden" name="frueh" value="" />
					<input type="hidden" name="spaet" value="" />
				</fieldset>
			</form>
		</div>
	</div>

	<script type="text/javascript">
        <?php
        echo "var jsonPlanStr='" . $plan->asJson() . "';";
        echo "var calendarMode='" . CALENDAR_MODE . "';";
        echo "var resourceStatus='" . RESOURCE_STATUS . "';";
        echo "var hints='" . HINTS . "';";        
        ?>
        </script>
	    <script type="text/javascript" src="jswindenfahrer.js"></script>
</body>

</html>
