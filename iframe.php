<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!-- Copyright 2018 Tobias Grundmann -->
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <title>Schleppbetrieb <?php echo date("Y",time())?> iframe test</title>
        <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    </head>

    <body>
<h1>iframe test</h1>
ohne scrolling
<p><iframe frameborder="0" height="1500" name="I1" scrolling="no" src="/windenfahrerplan.php" translate="no" width="2000">Ihr Browser unterst&uuml;tzt Inlineframes nicht oder zeigt sie in der derzeitigen Konfiguration nicht an.</iframe></p>

mit scrolling
<p><iframe frameborder="0" height="1500" name="I1" scrolling="yes" src="/windenfahrerplan.php" translate="no" width="2000">Ihr Browser unterst&uuml;tzt Inlineframes nicht oder zeigt sie in der derzeitigen Konfiguration nicht an.</iframe></p>

    </body>

</html>
