<?php
//Copyright 2018,2021,2023 Tobias Grundmann
//
//This file is part of Windenfahrerplan.
//
//Windenfahrerplan is free software: you can redistribute it and/or modify
//it under the terms of version 3 of the GNU General Public License as
//published by the Free Software Foundation
//
//Windenfahrerplan is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Windenfahrerplan.  If not, see <http://www.gnu.org/licenses/>.

error_reporting(E_ALL);
class Weather {
        public $weatherResponse;
        public $error;

        public $fetchTime;
        public $hourlyTimeArray = array(); //Store JSON date/time-Strings as DateTime Objects
        
        private static $file = "data/weather";
        private static $localTimezone = "Europe/Berlin";

        function getEncodedIntervalsForDay($day) {
                $date = $day->asDate();
                $date = $date->setTimezone(new DateTimeZone(Weather::$localTimezone));

                $hourlyWindspeed10m = $this->weatherResponse["hourly"]["windspeed_10m"];
                $hourlyWinddirection10m = $this->weatherResponse["hourly"]["winddirection_10m"];
                $hourlyWindgusts10m = $this->weatherResponse["hourly"]["windgusts_10m"];
                $hourlyPrecipitation = $this->weatherResponse["hourly"]["precipitation"];
                
                $hours = array(9,12,15,18);
                $wind = "";
                $gusts = "";
                $precipitation = "";
                $weatherDiv = "<div class='weather_grid'>";

                foreach ($hours as $hour) {
                        $date = $date->setTime($hour,0,0);
                        //Can't assume hour index for hour x in response is also x so search index in array
                        $indexHour = array_search($date,$this->hourlyTimeArray);
                        $hourStr = $date->format("H\h T");
                        if($indexHour !== false) {
                                $wind          .= $this->windRep($hourlyWindspeed10m[$indexHour],$hourlyWinddirection10m[$indexHour],$hourStr);
                                $gusts         .= $this->gustsRep($hourlyWindgusts10m[$indexHour],$hourStr);
                                $precipitation .= $this->precRep($hourlyPrecipitation[$indexHour],$hourStr);
                        } else {
                                $wind          .= $this->noWindData($hourStr);
                                $gusts         .= $this->noGustData($hourStr);
                                $precipitation .= $this->noPrecData($hourStr);
                        }
                }
                $weatherDiv .= $wind;
                $weatherDiv .= $gusts;
                $weatherDiv .= $precipitation;
                $weatherDiv .= "</div>";
                
                return $weatherDiv;
        }
        
        function precRep($precipitation,$hour) {
                if ($precipitation !== false) {
                        $blue = "";
                        if ($precipitation > 0) {
                                $blue = "weatherblue";
                        }
                        if ($precipitation > 1) {
                                $precipitation = round($precipitation);
                        }
                        $title = "Niederschlag " . $hour . ": " . $precipitation . " &#13212;";
                        return "<div class='prec_div weather_div $blue'>" . $this->nFormat($precipitation) . "<span class='weathertext'>$title</span></div>";
                } else {
                        return $this->noPrecData($hour); 
                }
        }
        
        function gustsRep($gustspeed,$hour) {
                if ($gustspeed !== null) {
                        $title = "Böen " . $hour . ": " . $this->nFormat($gustspeed) . " &#13214;/h, " . $this->nFormat(round($this->knots($gustspeed),1)) . " kt";
                        $color = $this->windColor($gustspeed);
                        $style = "background-color:$color";
                        return "<div class='gust_div weather_div' style='$style'>" . $this->nFormat(round($gustspeed)) . "<span class='weathertext'>$title</span></div>";
                } else {
                        return $this->noGustData($hour); 
                }
        }
        function nFormat($number) {
                $number = $number ?? 0;
                $decimals = $number == round($number) ? 0 : 1;
                return number_format($number,$decimals,",");
        }
        function windRep($windspeed,$winddirection,$hour) {
                if ($windspeed !== false && $winddirection !== false) {
                        $title = "Wind " . $hour . ": " . $this->nFormat($windspeed) . " &#13214;/h, " . $this->nFormat(round($this->knots($windspeed),1)) . " kt, " . $winddirection . "°";
                        $windbarb = $this->windbarb($windspeed);
                        $color = $this->windColor($windspeed);
                        $style = "background-color:$color";
                        return "<div class='wind_div weather_div' style='$style'>" . "<img src='$windbarb' class='weather_img' style='transform:rotate(" . ($winddirection - 270) . "deg)'/>" . "<span class='weathertext'>$title</span></div>";
                } else {
                        return $this->noWindData($hour); 
                }
        }
        function noWindData($hour) {
                return "<div title='$hour. Keine Winddaten' style='background-color:#DD9999'></div>"; 
        }
        function noGustData($hour) {
                return "<div title='$hour. Keine Böendaten' style='background-color:#DD9999'></div>"; 
        }
        function noPrecData($hour) {
                return "<div title='$hour. Keine Niederschlagsdaten' style='background-color:#DD9999'></div>"; 
        }
        
        function windcolor($kmh) {
                $roundedTo3kmh=round($kmh / 3) * 3;
                switch($roundedTo3kmh) {
                case 33: return "#ff000055";
                case 30: return "#ff400055";
                case 27: return "#ff800055";
                case 24: return "#ffbf0055";
                case 21: return "#ffff0055";
                case 18: return "#bfff0055";
                case 15: return "#80ff0055";
                case 12: return "#40ff0055";
                case 9:  return "#00ff8055";
                case 6:  return "#00ffbf55";
                case 3:  return "#00ffff55";
                case 0:  return "#ffffff55";
                default: return "#ff000055";
                }
        }

        function knots($kmh) {
                return fdiv($kmh ?? 0.0,1.852);
        }
        function windbarb($kmh) {
                $knots = $this->knots($kmh);
                $roundedTo5Knots=round($knots / 5) * 5;
                
                switch($roundedTo5Knots) {
                case 0:  return "weather_icons/wind_speed_00.svg";
                case 5:  return "weather_icons/wind_speed_01.svg";
                case 10: return "weather_icons/wind_speed_02.svg";
                case 15: return "weather_icons/wind_speed_03.svg";
                case 20: return "weather_icons/wind_speed_04.svg";
                case 25: return "weather_icons/wind_speed_05.svg";
                case 30: return "weather_icons/wind_speed_06.svg";
                case 35: return "weather_icons/wind_speed_07.svg";
                case 40: return "weather_icons/wind_speed_08.svg";
                case 45: return "weather_icons/wind_speed_09.svg";
                case 50: return "weather_icons/wind_speed_10.svg";
                case 55: return "weather_icons/wind_speed_11.svg";
                case 60: return "weather_icons/wind_speed_12.svg";
                case 65: return "weather_icons/wind_speed_13.svg";
                case 70: return "weather_icons/wind_speed_14.svg";
                case 75: return "weather_icons/wind_speed_15.svg";
                default: return "weather_icons/wind_speed_15.svg";
                }
        }
        
        function save() {
                file_put_contents(Weather::$file,serialize($this),LOCK_EX);                
                file_put_contents(Weather::$file . ".json",json_encode($this,JSON_PRETTY_PRINT),LOCK_EX);                
        }
        
        public static function load() {
                $weather = null;
                if( file_exists(Weather::$file)) {
                        $weather = unserialize(file_get_contents(Weather::$file,LOCK_EX));
                        $now = new DateTimeImmutable('now');
                        if ($now->modify("-1 hours") > $weather->fetchTime) {
                                $weather = null;
                        } else if ($weather->error != null && $now->modify("-10 minutes") > $weather->fetchTime) {
                                $weather = null;
                        }
                } 
                if (!$weather) {
                        error_log("fetching new weather");
                        $weather = Weather::httpGet();
                        $weather->fetchTime = new DateTimeImmutable('now');
                        if ($weather->error == null) {
                                $timezone = $weather->weatherResponse["timezone"];
                                //doesn't work - makes array with empty entry in front - why?
                                //$weather->$hourlyTimeArray = array_map(fn($v) : DateTime => new DateTime($v,new DateTimeZone(Weather::toPhpTimezone($timezone))),$weather->weatherResponse["hourly"]["time"]);
                                
                                foreach($weather->weatherResponse["hourly"]["time"] as $time) {
                                        $weather->hourlyTimeArray[] = new DateTime($time,new DateTimeZone(Weather::toPhpTimezone($timezone)));
                                }
                        }
                        $weather->save();
                        //error_log("weather [" . json_encode($weather)  . "]");
                }
                return $weather;
        }

        static function toPhpTimezone($timezone) {
                if ($timezone == "GMT" || $timezone == null) {
                        return "UTC";
                } else {
                        return $timezone;
                }
        }
        
        static function httpGet() {
                $curl = curl_init();
                $url = "https://api.open-meteo.com/v1/forecast?latitude=" . LATITUDE . "&longitude=" . LONGITUDE . "&hourly=precipitation,windspeed_10m,windgusts_10m,winddirection_10m&models=icon_seamless";
                error_log($url);
                curl_setopt_array($curl, [
                        CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                ]);

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                $weather = new Weather();
                if ($err) {
                        $weather->error = $err;
                } else {
                        $weather->weatherResponse = json_decode($response, true);
                }  
                return $weather;
        }
}
?>
