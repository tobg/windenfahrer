<?php
// CALENDAR_MODE legt fext, ob fixe Termine angelegt werden können oder ein fliessender Kalender angezeigt wird
// Werte: "fixed", "floating" (default)
define("CALENDAR_MODE", "floating");

// Wochentage, die im Modus 'floating' durchgeschaltet werden
define("WEEKDAYS", array (
                'Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday',
                'Sunday'
));

//Anzahl der angezeigten Tage bzw. maximale Anzahl an Terminen
define("NUM_DAYS", 14);

// Sollen Wetterinformationen angezeigt werden
define("WEATHER", true);
// Koordination für den Wetterabruf bei openmeteo
define("LATITUDE","52.00");
define("LONGITUDE","13.00");

// Soll der Status bestimmer Ressourcen als Ampel angezeigt werden
define("RESOURCE_STATUS", true);

// welche Resourcen sollen als Ampel angezeigt werden
define("RESOURCES_WITH_STATUS", array (
                'Felix 1',
                'Felix 2',
                'Mohaupt',
                'Koch',
));

// Einträge in $specialParticipants werden immer nach oben sortiert in der Reihenfolge die durch die Zahlen bestimmt ist.
// Außerdem werden sie beim ersten Start automatisch angelegt und nach Ablauf der automatischen Löschfrist nicht gelöscht.
// Wenn kein Bedarf besteht leer definieren: array()
define ("SPECIAL_PARTICIPANTS", array (
                'Schleppbetrieb' => 1
                // 'Flugwetter' => 2,
));

// Sollen die Hinweise angezeigt werden?
define("HINTS", true);

// Titel (Titel des Browserfensters)
define("HTML_TITLE", "Schleppbetrieb Altes Lager");  

// Überschrift
define("HEADER", "Windenschlepp in Altes Lager");  

// Motto
define("MOTTO", "Das fliegt");  

// Logobild
define("LOGO", "52N13E.svg");

// Die Zeit in Tagen nach der inaktive Teilnehmer gelöscht werden
define("MAX_INACTION_DAYS", 100);

// Gibt interne Daten, insbesondere IDs und POST Information mit auf der Oberfläche aus
define("DEBUG", false);
?>
