"use strict";
//Copyright 2018,2021 Tobias Grundmann
//
//This file is part of Windenfahrerplan.
//
//Windenfahrerplan is free software: you can redistribute it and/or modify
//it under the terms of version 3 of the GNU General Public License as
//published by the Free Software Foundation
//
//Windenfahrerplan is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Windenfahrerplan.  If not, see <http://www.gnu.org/licenses/>.


var jsonPlan = JSON.parse(jsonPlanStr);
var cellContent = { '+': '&#x2713;', '~': '?', '-': '&times;', '_': '&nbsp;' };

console.log("calendarMode [" + calendarMode + "]");

if (resourceStatus) {
	styleStaticResources();
}

styleCells();
if (hints) {
	initHinweisText();
	countChars('hinweisText', 'charcount');
}

document.getElementById('neuerFahrerButton').addEventListener('click', fahrerFensterOpen);
document.getElementById('closeNeuerFahrer').addEventListener('click', function() { fensterClose('neuerFahrerPopup'); });
document.getElementById('fahrerLoeschen').addEventListener('click', fahrerLoeschen);
if (hints) {
	document.getElementById('hinweisButton').addEventListener('click', hinweisFensterOpen);
	document.getElementById('closeHinweis').addEventListener('click', function() { fensterClose('hinweisPopup'); });
	document.getElementById('hinweisText').addEventListener('focus', function() { countChars('hinweisText', 'charcount'); });
	document.getElementById('hinweisText').addEventListener('keyup', function() { countChars('hinweisText', 'charcount'); });
	document.getElementById('hinweisText').addEventListener('keydown', function() { countChars('hinweisText', 'charcount'); });
}
if (calendarMode == 'fixed') {
	document.getElementById('terminButton').addEventListener('click', terminFensterOpen);
	document.getElementById('closeTermin').addEventListener('click', function() { fensterClose('terminPopup'); });
	document.getElementById('terminLoeschen').addEventListener('click', terminLoeschen);
}

window.onclick = function(event) {
	var modal = document.getElementById('modal');
	if (event.target == modal) {
		fensterClose('neuerFahrerPopup');
		if (hints) {
			fensterClose('hinweisPopup');
		}
		if (calendarMode == 'fixed') {
			fensterClose('terminPopup');
		}
	}
	closeDropdowns(event);
}

function countChars(where, displayIn) {
	var len = document.getElementById(where).value.length;
	var counter = document.getElementById(displayIn);
	counter.innerHTML = len;
	if (len > jsonPlan.hinweisMaxLen - 10) {
		counter.style.color = "red";
	}
	//console.log('countChars: ' + len + ' of ' + jsonPlan.hinweisMaxLen);
}

function fahrerLoeschen() {
	var form = document.getElementById('neuerFahrerForm');
	form.action.value = 'fahrerLoeschen';
}

function fahrerFensterOpen(fahrer, action) {
	var popup = document.getElementById('neuerFahrerPopup');
	var modal = document.getElementById('modal');
	var loeschen = document.getElementById('fahrerLoeschen');
	var positionRect;
	popup.className = 'overlay';
	modal.style.display = 'block';

	var form = document.getElementById('neuerFahrerForm');
	var legend = document.getElementById('fahrerPopupH');
	if (action == 'fahrerAendern') {
		loeschen.style.display = 'inline-block';
		form.action.value = 'fahrerAendern';
		form.fahrerId.value = fahrer['id'];
		form.fahrerName.value = fahrer['name'];
		form.windenfahrerGS.checked = fahrer['windenfahrerGS'];
		form.windenfahrerHG.checked = fahrer['windenfahrerHG'];
		form.EWF.checked = fahrer['EWF'];
		legend.innerHTML = 'Angaben f&uuml;r ' + fahrer['name'];
		positionRect = document.getElementById(fahrer['id']).getBoundingClientRect();
	} else {
		loeschen.style.display = 'none';
		form.action.value = 'neuerFahrer';
		form.fahrerId.value = '';
		form.fahrerName.value = '';
		form.windenfahrerGS.checked = false;
		form.windenfahrerHG.checked = false;
		form.EWF.checked = false;
		legend.innerHTML = 'Neuer Teilnehmer';
		positionRect = document.getElementById('neuerFahrerButton').getBoundingClientRect();
	}
	//console.log('pos (' + positionRect.top + ' ' + positionRect.left + ' ' + popup.getBoundingClientRect().height + ')');
	//Fensterposition, so dass auch auf Mobilgeräten mit vergrößerter Ansich das Fenster im sichbaren Bereich aufgeht
	//style ist sticky
	popup.style.top = positionRect.top - popup.getBoundingClientRect().height + 'px';
	popup.style.left = positionRect.left + 'px';
	//geht nicht!?
	//popup.scrollIntoView();
}

function initHinweisText() {
	var form = document.getElementById('hinweisForm');
	if (jsonPlan.aktuelleHinweise) {
		form.hinweisText.value = jsonPlan.aktuelleHinweise;
	}
}

function hinweisFensterOpen() {
	var popup = document.getElementById('hinweisPopup');
	var modal = document.getElementById('modal');
	popup.className = 'overlay';
	modal.style.display = 'block';
	initHinweisText();
	countChars('hinweisText', 'charcount');
}

function terminLoeschen() {
	var form = document.getElementById('terminForm');
	form.action.value = 'terminLoeschen';
}

function terminFensterOpen(termin, action) {
	console.log("terminFensterOpen");
	//console.debug(termin);
	var popup = document.getElementById('terminPopup');
	var modal = document.getElementById('modal');
	var loeschen = document.getElementById('terminLoeschen');
	var form = document.getElementById('terminForm');
	var positionRect;

	popup.className = 'overlay';
	modal.style.display = 'block';
	if (action == 'terminAendern') {
		loeschen.style.display = 'inline-block';
		//form.datum.disabled=true;
		form.terminId.value = termin['id'];
		form.terminTitel.value = termin['titel'];
		form.datum.value = termin['datum'];
		form.action.value = 'terminAendern';
		positionRect = document.getElementById(termin['id']).getBoundingClientRect();
	} else {
		loeschen.style.display = 'none';
		//form.datum.disabled=false;
		form.action.value = 'neuerTermin';
		form.terminId.value = '';
		form.terminTitel.value = '';
		form.datum.value = '';
		positionRect = document.getElementById('terminButton').getBoundingClientRect();
	}
	popup.style.top = positionRect.top - popup.getBoundingClientRect().height + 'px';
	popup.style.left = positionRect.left + 'px';

}

function fensterClose(id) {
	var popup = document.getElementById(id);
	var modal = document.getElementById('modal');
	popup.className = 'overlayHidden';
	modal.style.display = 'none';
}

function styleCellBackground(cell, tag) {
	//var farben = {'+':'#55FF55','~':'#EEEE00','-':'#CC0000', '_' : '#cdeeFF'};
	var farben = { '+': '#55EC55BC', '~': '#DDDD00BC', '-': '#CA4444BC', '_': 'rgb(0, 0, 0, 0)' };
	cell.style.background = 'linear-gradient(90deg,' + farben[tag['frueh']] + ',' + farben[tag['frueh']] + ',' + farben[tag['spaet']] + ',' + farben[tag['spaet']] + ')';
}

function contains(ar, el) {
	return ar.indexOf(el) != -1;
}

function closeDropdowns(event) {
	var dropdowns = document.getElementsByClassName('dropdown');
	for (var i = 0; i < dropdowns.length; i++) {
		if (!event.ignoreMe || !contains(event.ignoreMe, dropdowns[i])) {
			dropdowns[i].parentNode.removeChild(dropdowns[i]);
		}
	}
	var dropdownConts = document.getElementsByClassName('dropdownContHover');
	for (var i = 0; i < dropdownConts.length; i++) {
		if (!event.ignoreMe || !contains(event.ignoreMe, dropdownConts[i])) {
			dropdownConts[i].classList.remove('dropdownContHover');
		}
	}
}

function submitRegistration(fahrer, tag) {
	//console.log('submit ' + fahrer.name + ' ' + tag.datum + tag.frueh + tag.spaet);
	var anmeldeform = document.getElementById('anmeldeform');
	anmeldeform.fahrerId.value = fahrer['id'];
	anmeldeform.datum.value = tag['datum'];
	anmeldeform.frueh.value = tag['frueh'];
	anmeldeform.spaet.value = tag['spaet'];
	//anmeldeform.submit();
	var request = new XMLHttpRequest();
	request.open("POST",  window.location.pathname);
	request.onreadystatechange = function() {
		if (request.readyState === 4) {
			if (request.status === 200) {
				//console.log(request.responseText)  
			} else {
				console.log("Error [" + request.statusText + "]");
				var error = document.getElementById('error');
				error.innerHTML = 'Fehler [' + request.statusText + '] bei der Daten&uuml;bertragung.';

			}
		}
	};

	request.send(new FormData(anmeldeform));
}

function makeDropdown(zeit, tag, fahrer, cell, spanToFill, dateStr) {
	var dropdown = document.createElement('div');
	dropdown.className = 'dropdown';

	//options = value : [html][css-class-extension]
	var options = { '+': ['&nbsp;&#x2713;&nbsp;', 'yes'], '~': ['&nbsp;?&nbsp;', 'maybe'], '-': ['&nbsp;&times;&nbsp;', 'no'], '_': ['<small>(kein&nbsp;Eintrag)</small>', 'noentry'] };
	var keys = ['+', '~', '-', '_']; //stable sort order
	for (var k in keys.reverse()) {
		var opt = keys[k];
		var dropdownOption = document.createElement('div');
		dropdownOption.classList.add('dropdownOption');
		dropdownOption.classList.add('dropdownOption_' + options[opt][1]);
		dropdownOption.classList.add('dropdownOption_' + options[opt][1] + '_' + zeit);

		var s = document.createElement('span');
		s.innerHTML = options[opt][0];
		dropdownOption.appendChild(s);
		(function() {
			var z = zeit;
			var t = tag;
			var f = fahrer;
			var d = dropdown;
			var c = cell;
			var s = spanToFill;
			var value = opt;
			dropdownOption.onclick = function(event) {
				t[z] = value;
				if (!event.ignoreMe) {
					event.ignoreMe = [];
				}
				event.ignoreMe.push(d);
				styleCellBackground(c, t);
				s.innerHTML = cellContent[value];
				submitRegistration(f, t);
			};
		})();
		dropdown.appendChild(dropdownOption);
	}
	var dropdownHeader = document.createElement('div');
	dropdownHeader.className = 'dropdownHeader';
	dropdownHeader.innerHTML = fahrer.name + '<br/>' + dateStr + ' ' + (zeit == 'frueh' ? 'fr&uuml;h' : 'sp&auml;t');
	dropdown.appendChild(dropdownHeader);

	return dropdown;
}

function makeDropdownOnClick(cell, zeit, tag, fahrer, spanToFill, dateStr) {

	return function(event) {
		//console.log(event.currentTarget.tagName + ' ' + fahrer.name);
		var dropdownCont = event.currentTarget;
		if (!event.ignoreMe) {
			event.ignoreMe = [];
		}
		//toggle dropdown
		var dropdowns = dropdownCont.getElementsByClassName('dropdown');
		if (dropdowns.length == 0) {
			var d = makeDropdown(zeit, tag, fahrer, cell, spanToFill, dateStr);
			dropdownCont.appendChild(d);
			event.ignoreMe.push(d);
		} else {
			for (var j = 0; j < dropdowns.length; j++) {
				dropdownCont.removeChild(dropdowns[j]);
			}
		}

		dropdownCont.classList.toggle('dropdownContHover');
		event.ignoreMe.push(dropdownCont);
	}
}

var invisibleRows = [];

//Da in Firefox visibility:collapse bewirkt, dass das Dropdown-Menu
//nicht mehr funktioniert (in Chrome geht's), statt visibility
//zu setzen, die Rows aushängen.
//Funktion kann mehrfach mit collapse aufgerufen werden.
//Deshalb das zweite Array 'poppedRows' zum Sammeln der entfernten Rows.
//Entfernte Rows dann jeweils im Block vor bisher bereits entfernte in invisbleRows einhaengen,
//so dass sie in der richtigen Reihenfolge wieder zurueckgeholt werden koennen.

function visibilityUntilFirstSibling(row, visibility, poppedRows) {
	//console.log('visibilityUntilFirstSibling' + ' ' + row.tagName + ' ' + visibility);

	if (visibility == 'collapse') {
		if (row.className == 'datumszeile') {
			invisibleRows = poppedRows.concat(invisibleRows);
			return;
		}
		var previousRow = row.previousElementSibling;
		poppedRows.push(row);
		row.parentNode.removeChild(row);
		visibilityUntilFirstSibling(previousRow, visibility, poppedRows);
	} else {
		if (invisibleRows.length == 0) {
			return;
		}
		var arrows = row.getElementsByClassName('arrow');
		if (arrows.length > 0) {
			arrows[0].innerHTML = '&uarr;';
		}
		var previousRow = invisibleRows.shift();
		row.parentNode.insertBefore(previousRow, row);
		visibilityUntilFirstSibling(previousRow, visibility);
	}
}

function closestAncestorElement(element, tagName) {
	if (element) {
		if (element.tagName == tagName) {
			return element;
		}
		return closestAncestorElement(element.parentElement, tagName);
	}
	return null;
}
function styleStaticResources() {

	for (var resKey in jsonPlan['staticResourcesWithStatus']) {
		var resValue = jsonPlan['staticResourcesWithStatus'][resKey];
		//console.log(' resKey' + resKey + ', resValue' + resValue);

		var resTD = document.getElementById(resKey);

		var lights = resTD.getElementsByClassName('light');
		for (var i = 0; i < lights.length; i++) {

			(function() {
				var mi = i;
				var mlights = lights;
				var mResKey = resKey;
				var mResValue = resValue;
				mlights[mi].onclick = function() {

					for (var j = 0; j < mlights.length; j++) {
						mlights[j].classList.remove('light_on');
						mlights[j].classList.add('light_off');
					}
					mlights[mi].classList.remove('light_off');
					mlights[mi].classList.add('light_on');

					if (mlights[mi].classList.contains('red')) {
						mResValue = '-';
					} else if (mlights[mi].classList.contains('yellow')) {
						mResValue = '~';
					} else if (mlights[mi].classList.contains('green')) {
						mResValue = '+';
					}
					console.log('click ' + mResKey + ' ' + mResValue);
					var request = new XMLHttpRequest();
					request.open("POST",  window.location.pathname);
					request.onreadystatechange = function() {
						if (request.readyState === 4) {
							if (request.status === 200) {
								//console.log(request.responseText)  
							} else {
								console.log("Error [" + request.statusText + "]");
								var error = document.getElementById('error');
								error.innerHTML = 'Fehler [' + request.statusText + '] bei der Daten&uuml;bertragung.';

							}
						}
					};
					var form = new FormData(form);
					form.set("action", "staticResourceChange");
					form.set("resourceID", mResKey);
					form.set("resourceValue", mResValue);
					request.send(form);
				}
			})();
		}
	}
}
function styleCells() {
	//Date formatting is expensive - so do it only once
	var dateStrings = [];
	for (var tagIx in jsonPlan['referenzBereitschaft']['tage']) {
		var tag = jsonPlan['referenzBereitschaft']['tage'][tagIx];
		var date = new Date(tag.datum);
		var dateOptions = { month: '2-digit', day: '2-digit' };
		var dateStr = date.toLocaleDateString("de-DE", dateOptions);
		dateStrings.push(dateStr);
	}
	if (calendarMode == 'fixed') {
		console.debug(jsonPlan['termine']);
		for (var terminIx in jsonPlan['termine']) {
			var termin = jsonPlan['termine'][terminIx];
			var terminCell = document.getElementById(termin['id']);
			(function() {
				var t = termin;
				var action = 'terminAendern';
				terminCell.onclick = function() {
					terminFensterOpen(t, action);
				};
			})();
		}
	}
	for (var bereitschaft in jsonPlan['bereitschaften']) {
		var fahrer = jsonPlan['bereitschaften'][bereitschaft]['fahrer'];
		var fahrerCell = document.getElementById(fahrer['id']);
		var fahrerArrow = document.getElementById('arrow_' + fahrer['id']);

		(function() {
			var f = fahrer;
			var action = 'fahrerAendern';
			if (fahrerCell.classList.contains('active')) {
				fahrerCell.onclick = function() {
					fahrerFensterOpen(f, action);
				};
			}
			if (fahrerArrow) {
				fahrerArrow.onclick = function(event) {
					var row = closestAncestorElement(event.currentTarget, 'TR');
					var arrows = event.currentTarget.parentNode.getElementsByClassName('arrow');

					//console.log('fahrer ' + f.name + ' row ' + row.id + ' arrows.length ' + arrows.length + ' event.currentTarget ' + event.currentTarget);
					if (arrows.length > 0) {//not in first data row
						if (row.previousElementSibling.className == 'datumszeile') {
							visibilityUntilFirstSibling(row, 'visible');
						} else {
							var poppedRows = [];
							visibilityUntilFirstSibling(row.previousElementSibling, 'collapse', poppedRows);
							arrows[0].innerHTML = '&#11021;';
						}
					}
				}
			}
		})();
		var dateIx = 0;
		for (var tagIx in jsonPlan['bereitschaften'][bereitschaft]['tage']) {
			var tag = jsonPlan['bereitschaften'][bereitschaft]['tage'][tagIx];
			var cell = document.getElementById(tag['id']);
			styleCellBackground(cell, tag);

			var dateStr = dateStrings[dateIx++];
			var zeiten = ['frueh', 'spaet'];
			for (var i = 0; i < 2; i++) {
				var dropdownCont = document.createElement('div');
				dropdownCont.className = 'dropdownCont';

				var span = document.createElement('span');
				span.innerHTML = cellContent[tag[zeiten[i]]];
				dropdownCont.appendChild(span);

				cell.appendChild(dropdownCont);

				dropdownCont.onclick = makeDropdownOnClick(cell, zeiten[i], tag, fahrer, span, dateStr);
			}
		}
	}
}
