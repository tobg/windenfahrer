<?php
// Copyright 2018,2021,2023 Tobias Grundmann
//
// This file is part of Windenfahrerplan.
//
// Windenfahrerplan is free software: you can redistribute it and/or modify
// it under the terms of version 3 of the GNU General Public License as
// published by the Free Software Foundation
//
// Windenfahrerplan is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Windenfahrerplan. If not, see <http://www.gnu.org/licenses/>.
error_reporting(E_ALL);

function uuid() {
        return bin2hex(random_bytes(16));
}

class Fahrer {
        public $name;
        public $id;
        public $windenfahrerGS = false;
        public $windenfahrerHG = false;
        public $EWF = false;
        public $lastAction;
        public $editable;

        function __construct($name, $id) {
                $this->name = $name;
                $this->id = 'f_' . $id;
        }

        function lastAction($datum) {
                $this->lastAction = $datum->format("Y-m-d");
        }

        function lastActionAsDate() {
                return DateTime::createFromFormat("!Y-m-d", $this->lastAction);
        }

        public function renderFeature() {
                if ($this->EWF) {
                        return "EWF";
                }
                if ($this->windenfahrerHG) {
                        return "WD";
                }
                if ($this->windenfahrerGS) {
                        return "W";
                }
                return "";
        }

        public function renderContent() {
                // $GS = $this->windenfahrerGS ? "G" : "-";
                // $HG = $this->windenfahrerHG ? "D" : "-";
                // $EWF = $this->EWF ? "E" : "-";
                $name = $this->name;
                // $debug = "<small>|" . $this->id . " (" . $GS . "," . $HG . "," . $EWF . "," . $this->lastAction . ")</small>";
                $debug = '';
                return $name . $debug;
        }
}

class Termin {

        function __construct($datum, $titel) {
                $this->datum = $datum->format("Y-m-d");
                $this->titel = $titel;
                $this->id = uuid();
        }
        public $id;
        public $titel;
        public $datum;

        public function asDate() {
                return DateTimeImmutable::createFromFormat("!Y-m-d", $this->datum);
        }

        public function render() {
                $debug = '';
                if (DEBUG) {
                        $debug = "<span style='font-size:50%'> [" . $this->id . "]</span>";
                }
                return $this->titel . $debug;
        }
}

class Tag {

        function __construct($datum, $id) {
                $this->datum = $datum->format("Y-m-d");
                $this->setId($id);
        }
        public $id;
        public $datum;
        public $frueh = "_";
        public $spaet = "_";

        public function setId($id) {
                $this->id = "tag_" . $id;
        }

        public function asDate() {
                return DateTimeImmutable::createFromFormat("!Y-m-d", $this->datum);
        }

        public function dm() {
                $wochentage = array (
                                'So',
                                'Mo',
                                'Di',
                                'Mi',
                                'Do',
                                'Fr',
                                'Sa'
                );
                $d = $this->asDate();
                return $wochentage[$d->format("w")] . " " . $d->format("d.m.");
        }

        public function renderContent() {
                $debug = '';
                if (DEBUG) {
                        $debug = "<span style='font-size:50%'> [" . $this->dm() . "," . $this->id . "]</span>";
                }
                return $debug;
        }
}

class Bereitschaft {
        public $fahrer;
        public $tage = array ();

        public function score() {
                $az_tage = count($this->tage);
                $specialScore = (15 * $az_tage) + 15;
                $name = $this->fahrer->name;
                if (array_key_exists($name, Plan::$specialParticipants)) {
                        return $specialScore + Plan::$specialParticipants[$name];
                }
                $start_tag = 0;
                $deadline = new DateTimeImmutable('today');

                if ($az_tage > 0 && $this->tage[0]->asDate() == $deadline) {
                        $now = new DateTimeImmutable('now');
                        $deadline = $deadline->add(new DateInterval(Plan::$dailySwitch));
                        $start_tag = $now->diff($deadline)->invert; // invert ist 1 wenn Zeitspanne negativ, sonst 0
                }
                // am höchsten gewertet wird der zeitnächste eintrag, gewichtet mit der art des eintrags
                $weight = array (
                                '+' => 7,
                                '~' => 3,
                                '-' => 1,
                                '_' => 0
                );
                for ($i = $start_tag; $i < $az_tage; $i++) {
                        if ($this->tage[$i]->frueh == '_' && $this->tage[$i]->spaet == '_') {
                                continue;
                        }
                        //15 = maximum weight per day + 1
                        $dayScore = (15 * ($az_tage - $i));
                        return $dayScore + $weight[$this->tage[$i]->frueh] + $weight[$this->tage[$i]->spaet];
                }
                return 0;
        }
}

class Plan {
        // Maximale Länge für Hinweistext (nicht static wg. json-encoding)
        public $hinweisMaxLen = 250;
        public $terminTitelMaxLen = 15;
        // Die Tageszeit nach der die Sortierreihenfolge der Teilnehmer von Einträgen der Folgetage bestimmt wird
        // d.h. der aktuelle Tag nicht mehr berücksichtigt wird
        public static $dailySwitch = 'PT17H00M';
        // Die Zeit in Tagen nach der inaktive Teilnehmer gelöscht werden
        public static $lastActionDelta = MAX_INACTION_DAYS;
        // Einträge in $specialParticipants werden immer nach oben sortiert in der Reihenfolge die durch die Zahlen bestimmt ist.
        // Außerdem werden sie beim ersten Start automatisch angelegt und nach Ablauf der automatischen Löschfrist nicht gelöscht.
        public static $specialParticipants = SPECIAL_PARTICIPANTS;
        // (wiederkehrende) Wochentage an denen Veranstaltungen z.B. Schleppbetrieb stattfinden können
        private static $weekdays = WEEKDAYS;
        
        private static $file = "data/bereitschaften";
        private static $maxFahrer = 250; // Maximale Anzahl an Teilnehmern
        private static $anzahlTage = NUM_DAYS;
        private static $anzahlFahrer = 50; // Für Tests mit zufallsgenerierten Fahrern: deren Anzahl
        public $staticResourcesWithStatus;
        public $error = false;
        public $errorMsg = "";
        public $termine = array ();
        public $bereitschaften = array ();
        public $referenzBereitschaft;
        public $aktuelleHinweise;
        public $aktuelleHinweiseDateTime;
        private $nextTagId = 0;
        private $nextFahrerId = 0;

        private static function today() {
                return new DateTimeImmutable('today');
        }

        private static function cmpBereitschaften($a, $b) {
                $c = $b->score() - $a->score();
                if ($c == 0) {
                        return strcasecmp($a->fahrer->name, $b->fahrer->name);
                }
                return $c;
        }

        public function staticResourceChanged($resourceID, $resourceValue) {
                $accept = array (
                                '+',
                                '-',
                                '~'
                );
                if (in_array($resourceValue, $accept, true)) {
                        $this->staticResourcesWithStatus[$resourceID] = $resourceValue;
                        $this->speichern();
                }
        }

        public function hinweis($text) {
                if (strlen($text) > $this->hinweisMaxLen) {
                        $this->error = true;
                        $this->errorMsg = "Der Text ist zu lang.";
                        return;
                } else {
                        $this->aktuelleHinweise = preg_replace("/[\n\r]/", "", $text);
                        $dt = new DateTimeImmutable('now');
                        $this->aktuelleHinweiseDateTime = $dt->format(DateTime::ATOM);
                        $this->speichern();
                }
        }

        public function getHinweisDateTimeFormatted() {
                if ($this->aktuelleHinweiseDateTime) {
                        $dt = DateTime::createFromFormat(DateTime::ATOM, $this->aktuelleHinweiseDateTime);
                        return $dt->format("d.m.Y H:i\h");
                } else {
                        return "";
                }
        }

        public function maxFahrername() {
                $max = 0;
                foreach ($this->bereitschaften as $bereitschaft) {
                        $len = strlen($bereitschaft->fahrer->name);
                        if ($len > $max) {
                                $max = $len;
                        }
                }
                return $max;
        }

        public function update($fahrerId, $datum, $spaet, $frueh) {
                foreach ($this->bereitschaften as $bereitschaft) {
                        if ($fahrerId == $bereitschaft->fahrer->id) {
                                $bereitschaft->fahrer->lastAction(new DateTimeImmutable('today'));
                                foreach ($bereitschaft->tage as $tag) {
                                        if ($datum == $tag->datum) {
                                                $tag->frueh = $frueh;
                                                $tag->spaet = $spaet;
                                                break 2;
                                        }
                                }
                        }
                }
                $this->sort();
                $this->speichern();
        }

        private function pruneInactiveParticipants() {
                $oldestAcceptableAction = Plan::today()->modify("-" . Plan::$lastActionDelta . " days");
                $this->bereitschaften = array_values(array_filter($this->bereitschaften, function ($b) use ($oldestAcceptableAction) {
                        if (array_key_exists($b->fahrer->name, Plan::$specialParticipants)) {
                                return true;
                        }

                        if ($b->fahrer->lastActionAsDate() < $oldestAcceptableAction) {
                                return false;
                        }
                        return true;
                }));
        }

        private static function weekdayNumOf($date) {
                return $date->format('w');
        }

        private static function weekdayOf($date) {
                return $date->format('l');
        }

        private static function numOfWeekday($weekday) {
                switch ($weekday) {
                        case 'Sunday' :
                                return 0;
                        case 'Monday' :
                                return 1;
                        case 'Tuesday' :
                                return 2;
                        case 'Wednesday' :
                                return 3;
                        case 'Thursday' :
                                return 4;
                        case 'Friday' :
                                return 5;
                        case 'Saturday' :
                                return 6;
                        default :
                                throw new Exception("[" . $weekday . "] is not a weekday");
                }
        }

        private static function cmpWeekdays($a, $b) {
                return Plan::numOfWeekday($a) - Plan::numOfWeekday($b);
        }

        private function getNextWeekdayFrom($datum, $weekdays) {
                usort($weekdays, array (
                                $this,
                                "cmpWeekdays"
                ));
                $day = Plan::weekdayOf($datum);
                if (!in_array($day, $weekdays, true)) {
                        foreach ($weekdays as $weekday) {
                                if (Plan::numOfWeekday($weekday) > Plan::numOfWeekday($day)) {
                                        return $weekday;
                                }
                        }
                        return $weekdays[0];
                }
                return $day;
        }

        private function getWeekdaysAfter($datum) {
                $wd = Plan::$weekdays;
                usort($wd, array (
                                $this,
                                "cmpWeekdays"
                ));
                $weekday = $this->getNextWeekdayFrom($datum, $wd);

                while ($wd[0] != $weekday) {
                        array_push($wd, array_shift($wd));
                }

                return $wd;
        }

        public function fillDays($bereitschaft, $datum) {
                $weekdays = $this->getWeekdaysAfter($datum);
                $weekdayIdx = 0;
                $nWeekdays = count($weekdays);
                $datum = $datum->modify("-1 day"); // allows to always use "next 'weekdayname'" in the loop
                while (count($bereitschaft->tage) < Plan::$anzahlTage) {
                        $datum = $datum->modify("next " . $weekdays[$weekdayIdx]);
                        $tag = new Tag($datum, $this->nextTagId++);
                        array_push($bereitschaft->tage, $tag);
                        $weekdayIdx = ($weekdayIdx + 1) % $nWeekdays;
                }
        }

        private function pruneAndSupplementStandbyStatus() {
                $today = Plan::today();
                $nextStartDay = null;
                $nextWeekday = Plan::getNextWeekdayFrom($today, Plan::$weekdays);
                if (Plan::weekdayOf($today) == $nextWeekday) {
                        $nextStartDay = $today;
                } else {
                        $nextStartDay = $today->modify('next ' . $nextWeekday);
                }

                while (count($this->termine) > 0 && $this->termine[0]->asDate() < $nextStartDay) {
                        array_shift($this->termine);
                }
                foreach ($this->bereitschaften as $bereitschaft) {
                        while (count($bereitschaft->tage) > 0 && $bereitschaft->tage[0]->asDate() < $nextStartDay) {
                                array_shift($bereitschaft->tage);
                        }
                        if ($this->isFloating()) {
                                $rest_tage = count($bereitschaft->tage);
                                if ($rest_tage > 0) {
                                        $datum = $bereitschaft->tage[$rest_tage - 1]->asDate()->modify("+1 day");
                                } else {
                                        $datum = $today;
                                }
                                $this->filldays($bereitschaft, $datum);
                        }
                }
        }

        public function prune() {
                $this->pruneInactiveParticipants();
                $this->pruneAndSupplementStandbyStatus();
        }

        private function fahrerName($fahrerId) {
                foreach ( $this->bereitschaften as $bereitschaft ) {
                        if ($bereitschaft->fahrer->id == $fahrerId) {
                                return $bereitschaft->fahrer->name;
                        }
                }
                return '';
        }
        private function checkIsSpecialParticipant($fahrerId) {
                if (array_key_exists($this->fahrerName($fahrerId),Plan::$specialParticipants)) {
                        $this->error = true;
                        $this->errorMsg = "Der Name ist gesperrt";
                        return true;
                }
                return false;
        }
        public function fahrerLoeschen($fahrerId) {
                if ($this->checkIsSpecialParticipant($fahrerId)) {
                        return;
                }
                $this->bereitschaften = array_values(array_filter($this->bereitschaften, function ($b) use ($fahrerId) {
                        if ($b->fahrer->id == $fahrerId) {
                                return false;
                        }
                        return true;
                }));
                $this->speichern();
        }

        public function fahrerAendern($fahrerId, $fahrerName, $windenfahrerGS, $windenfahrerHG, $EWF) {
                if (!$this->checkName($fahrerName)) {
                        return;
                }
                if ($this->checkIsSpecialParticipant($fahrerId)) {
                        return;
                }
                foreach ($this->bereitschaften as $bereitschaft) {
                        $fahrer = $bereitschaft->fahrer;
                        if ($fahrer->name == $fahrerName && $fahrer->id != $fahrerId) {
                                $this->error = true;
                                $this->errorMsg = "Der Name ist bereits vergeben";
                                return;
                        }
                }

                foreach ($this->bereitschaften as $bereitschaft) {
                        if ($fahrerId == $bereitschaft->fahrer->id) {
                                $fahrer = $bereitschaft->fahrer;
                                $fahrer->lastAction(Plan::today());
                                if (isset($fahrerName) && $fahrerName != '') {
                                        $fahrer->name = $fahrerName;
                                }
                                $fahrer->windenfahrerGS = isset($windenfahrerGS) ? true : false;
                                $fahrer->windenfahrerHG = isset($windenfahrerHG) ? true : false;
                                $fahrer->EWF = isset($EWF) ? true : false;
                        }
                }
                $this->speichern();
        }

        public function checkName($fahrerName) {
                if (!(strlen($fahrerName) >= 3 && strlen($fahrerName) <= 25) || !preg_match('/^[-_0-9\p{L}\s\'\.]+$/u', $fahrerName)) {
                        $this->error = true;
                        $this->errorMsg = "Der Name ist ung&uuml;ltig";
                        return false;
                }
                return true;
        }
        public function checkTerminTitel($titel) {
                if (!(strlen($titel) <= $this->terminTitelMaxLen) || !preg_match('/^[-_0-9\p{L}\s\'\.\?!()&;:+*]*$/u', $titel)) {
                        $this->error = true;
                        $this->errorMsg = "Der Titel ist ung&uuml;ltig";
                        return false;
                }
                return true;
        }
        
        public function checkDatumFormat($datum) {
                if (!preg_match('/^\d{4}-\d{2}-\d{2}$/u', $datum)) {
                        $this->error = true;
                        $this->errorMsg = "Das Datum [" . $datum . " ] ist ung&uuml;ltig";
                        return false;
                }
                try {
                        new DateTime($datum);
                } catch (Exception $ex) {
                        error_log($ex->getMessage());
                        $this->error = true;
                        $this->errorMsg = "Das Datum ist ung&uuml;ltig";
                        return false;
                }
                return true;
        }

        public function neuerFahrer($fahrerName, $windenfahrerGS, $windenfahrerHG, $EWF) {
                if (!$this->checkName($fahrerName)) {
                        return;
                }
                if (count($this->bereitschaften) >= Plan::$maxFahrer) {
                        $this->error = true;
                        $this->errorMsg = "Die maximale Anzahl an Teilnehmern ist erreicht";
                        return;
                }
                foreach ($this->bereitschaften as $bereitschaft) {
                        $fahrer = $bereitschaft->fahrer;
                        if ($fahrer->name == $fahrerName) {
                                $this->error = true;
                                $this->errorMsg = "Der Name ist bereits vergeben";
                                return;
                        }
                }
                $fahrer = new Fahrer($fahrerName, $this->nextFahrerId++);
                $fahrer->lastAction(Plan::today());
                $fahrer->windenfahrerGS = isset($windenfahrerGS) ? true : false;
                $fahrer->windenfahrerHG = isset($windenfahrerHG) ? true : false;
                $fahrer->EWF = isset($EWF) ? true : false;
                $bereitschaft = new Bereitschaft();
                $bereitschaft->fahrer = $fahrer;

                if ($this->isFloating()) {
                        $this->fillDays($bereitschaft, Plan::today());
                } else {
                        $this->fillDates($bereitschaft, $this->termine);
                }

                array_push($this->bereitschaften, $bereitschaft);
                $this->speichern();
        }

        public function addSpecialParticipants() {
                error_log("addSpecialParticipants");              
                $i = 0;
                foreach ( array_keys(Plan::$specialParticipants) as $special ) {
                        $this->bereitschaften[$i] = new Bereitschaft();
                        $this->bereitschaften[$i]->fahrer = new Fahrer($special, $this->nextFahrerId++);
                        if ($this->isFloating()) {
                                $this->filldays($this->bereitschaften[$i], Plan::today());
                        }
                        $i++;
                }
        }

        public function random() {
                $status = array (
                                '+',
                                '-',
                                '~'
                );
                $full = true;

                $daysBack = ($full ? 0 : rand(0, 6));
                for ($i = 0; $i < Plan::$anzahlFahrer; $i++) {
                        $this->bereitschaften[$i] = new Bereitschaft();
                        $this->bereitschaften[$i]->fahrer = new Fahrer(sprintf("%'.04d Fahrer", $i), $this->nextFahrerId++);

                        $datum = Plan::today()->modify("-" . $daysBack . " days");
                        $this->bereitschaften[$i]->fahrer->lastAction($datum);
                        if ($full || rand(0, 1)) {
                                $this->bereitschaften[$i]->fahrer->windenfahrerGS = rand(0, 1);
                                $this->bereitschaften[$i]->fahrer->windenfahrerHG = rand(0, 1);
                                $this->bereitschaften[$i]->fahrer->EWF = rand(0, 1);
                        }
                        $datum = new DateTimeImmutable('-' . $daysBack . ' days');
                        $hasEntries = $full || rand(0, 1);
                        for ($j = 0; $j < Plan::$anzahlTage; $j++) {
                                $tag = new Tag($datum, $this->nextTagId++);
                                $this->bereitschaften[$i]->tage[$j] = $tag;

                                if ($hasEntries && ($full || rand(0, 1))) {
                                        $this->bereitschaften[$i]->tage[$j]->frueh = $status[$i < 10 ? 0 : rand(0, 2)];
                                        $this->bereitschaften[$i]->tage[$j]->spaet = $status[$i < 10 ? 0 : rand(0, 2)];
                                }
                                $datum = $datum->modify("+24 hours");
                        }
                }
                $this->referenzBereitschaft = new Bereitschaft();
                $this->fillDays($this->referenzBereitschaft, Plan::today()->modify("-" . $daysBack . " days"));

                $this->sort();
                $this->speichern();
        }

        public function resetIds() {
                $this->nextTagId = 0;
                foreach ($this->bereitschaften as $bereitschaft) {
                        foreach ($bereitschaft->tage as $tag) {
                                $tag->setId($this->nextTagId++);
                        }
                }
        }

        public function sort() {
                usort($this->bereitschaften, array (
                                $this,
                                "cmpBereitschaften"
                ));
                $this->resetIds();
        }

        public function speichern() {
                error_log("speichern [" . Plan::$file . "]");                
                file_put_contents(Plan::$file, serialize($this), LOCK_EX);
                file_put_contents("data/json", json_encode($this, JSON_PRETTY_PRINT), LOCK_EX);
        }

        public function isFloating() {
                return CALENDAR_MODE != 'fixed';
        }

        public static function laden() {
                error_log("laden [" . Plan::$file . "]");
                if (file_exists(Plan::$file)) {
                        $plan = unserialize(file_get_contents(Plan::$file, LOCK_EX));
                        $plan->prune();
                } else {
                        $plan = new Plan();
                        $plan->addSpecialParticipants();
                        $plan->speichern();
                }
                $plan->referenzBereitschaft = new Bereitschaft();
                if ($plan->isFloating()) {
                        $plan->fillDays($plan->referenzBereitschaft, Plan::today());
                } else {
                        $plan->fillDates($plan->referenzBereitschaft, $plan->termine);
                }
                $plan->sort();
                //error_log(var_export(RESOURCES_WITH_STATUS,true));
                
                if (!isset($plan->staticResourcesWithStatus) || 
                                count(array_diff(RESOURCES_WITH_STATUS, array_keys($plan->staticResourcesWithStatus))) > 0 ||
                                count(array_diff(array_keys($plan->staticResourcesWithStatus),RESOURCES_WITH_STATUS)) > 0 
                                ) {
                        $plan->staticResourcesWithStatus = array();
                        foreach (RESOURCES_WITH_STATUS as $resource) {
                                $plan->staticResourcesWithStatus[$resource] = '+';                                
                        }
                        $plan->speichern();
                }
                return $plan;
        }

        public function asJson() {
                return json_encode($this, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT);
        }

        public function asJsonToClient() {
                header('Content-Type: application/json');
                echo json_encode($this, JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_PRETTY_PRINT);
        }

        private function checkDatumAcceptable($datum) {
                if ($this->checkDatumFormat($datum) == false) {
                        return false;
                }
                foreach ($this->termine as $termin) {
                        if ($termin->datum == $datum) {
                                $this->error = true;
                                $this->errorMsg = "Ein Termin an diesem Tag existiert schon";
                                return false;
                        }
                }

                $datum = DateTimeImmutable::createFromFormat("!Y-m-d", $datum);
                if ($datum < Plan::today()) {
                        error_log("[" . $datum->format("Y-m-d") . "] is before today [" . Plan::today()->format("Y-m-d") . "]");
                        $this->error = true;
                        $this->errorMsg = "Termin liegt in der Vergangenheit";
                        return false;
                }
                return true;
        }

        private function neuerTermin_($datum, $terminTitel) {
                $datum = DateTimeImmutable::createFromFormat("!Y-m-d", $datum);
                $offset = 0;
                while ($offset < count($this->termine) && $datum > $this->termine[$offset]->asDate()) {
                        $offset = $offset + 1;
                }
                array_splice($this->termine, $offset, 0, array (
                                new Termin($datum, $terminTitel)
                ));
                array_splice($this->referenzBereitschaft->tage, $offset, 0, array (
                                new Tag($datum, $this->nextTagId++)
                ));
                foreach ($this->bereitschaften as $bereitschaft) {
                        array_splice($bereitschaft->tage, $offset, 0, array (
                                        new Tag($datum, $this->nextTagId++)
                        ));
                }
        }

        public function neuerTermin($datum, $terminTitel) {
                $terminTitel = substr($terminTitel, 0, $this->terminTitelMaxLen);
                error_log("neuer Termin [" . $datum . "] [" . $terminTitel . "] ");
                if ($this->isFloating()) {
                        error_log("Calendar not in fixed mode");
                        return;
                }
                if (count($this->termine) >= Plan::$anzahlTage) {
                        error_log("To many dates");
                        $this->error = true;
                        $this->errorMsg = "Die maximale Anzahl an Terminen ist erreicht";
                        return;
                }
                if ($this->checkDatumAcceptable($datum) == false) {
                        return;
                }
                if (!$this->checkTerminTitel($terminTitel)) {
                        return;
                }
                
                $this->neuerTermin_($datum, $terminTitel);
                $this->speichern();
        }

        public function fillDates($bereitschaft, $termine) {
                foreach ( $this->termine as $termin ) {
                        $tag = new Tag($termin->asDate(), $this->nextTagId++);
                        array_push($bereitschaft->tage, $tag);
                }
        }

        private function terminLoeschen_($offset) {
                array_splice($this->termine, $offset, 1);
                array_splice($this->referenzBereitschaft->tage, $offset, 1);
                foreach ( $this->bereitschaften as $bereitschaft ) {
                        array_splice($bereitschaft->tage, $offset, 1);
                }
        }

        private function checkTerminDeletable($offset) {
                foreach ( $this->bereitschaften as $bereitschaft ) {
                        if ($bereitschaft->tage[$offset]->frueh != '_' || $bereitschaft->tage[$offset]->spaet != '_') {
                                $this->error = true;
                                $this->errorMsg = "Termin ist schon angenommen";
                                return false;
                        }
                }
                return true;
        }

        public function terminLoeschen($terminId) {
                error_log("Termin löschen " . $terminId);
                if ($this->isFloating()) {
                        error_log("Calendar not in fixed mode");
                        return;
                }
                $offset = $this->terminOffset($terminId);
                if ($offset == count($this->termine)) {
                        error_log("Termin [" . $terminId . "] not in array");
                        return;
                }
                if ($this->checkTerminDeletable($offset) == false) {
                        return;
                }

                $this->terminLoeschen_($offset);
                $this->speichern();
                return;
        }

        /**
         *
         * @param
         *                terminId
         */
        private function terminOffset($terminId) {
                $offset = 0;
                while ($offset < count($this->termine) && $this->termine[$offset]->id != $terminId) {
                        $offset = $offset + 1;
                }
                return $offset;
        }

        public function terminAendern($terminId, $terminTitel, $datum) {
                error_log("Termin ändern [" . $terminId . "][". $terminTitel ."][" . $datum ."]" );
                if ($this->isFloating()) {
                        error_log("Calendar not in fixed mode");
                        return;
                }
                if (!$this->checkTerminTitel($terminTitel)) {
                        return;
                }
                
                $datumAendern = false;
                foreach ( $this->termine as $termin ) {
                        if ($termin->id == $terminId) {
                                $termin->titel = $terminTitel;
                                if ($termin->datum != $datum) {
                                        $datumAendern = true;
                                }
                                break;
                        }
                }
                if ($datumAendern) {
                        if ($this->checkDatumAcceptable($datum) == false) {
                                return;
                        }
                        $offset = $this->terminOffset($terminId);
                        if ($this->checkTerminDeletable($offset) == false) {
                                return;
                        }                        
                        $this->terminLoeschen_($offset);
                        $this->neuerTermin_($datum, $terminTitel);
                }
                $this->speichern();
        }
}
?>
