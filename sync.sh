dist_dir=dist
dist_zip=windenfahrerplan.zip
date=`date +%F_%H-%M`
git_ref=`git show-ref --verify refs/heads/master --hash`
versioned_css=${date}_styles.css
versioned_js=${date}_jswindenfahrer.js

if [ -d $dist_dir ]; then
    rm -vr $dist_dir
fi
rm $dist_zip
mkdir -vp $dist_dir/css
mkdir -vp $dist_dir/data

cp -vr example_config_fix.php CHANGELOG 52N13E.svg favicon.ico windenfahrerplan.php plan.php weather.php weather_icons config.php $dist_dir
cp -v jswindenfahrer.js $dist_dir/$versioned_js
cp -v css/styles.css $dist_dir/css/$versioned_css
echo $date

sed -i "s|href=\"css/styles.css\"|href=\"css/$versioned_css\"|g" $dist_dir/windenfahrerplan.php
sed -i "s|jswindenfahrer.js|$versioned_js|g" $dist_dir/windenfahrerplan.php
sed -i "s|<span class=\"version\"></span>|<span class=\"version\">$date $git_ref</span>|g" $dist_dir/windenfahrerplan.php

cp sftp.batch sftp_tmp.batch
sed -i "s|@dist_dir@|$dist_dir|g" sftp_tmp.batch
sed -i "s|styles.css|$versioned_css|g" sftp_tmp.batch
sed -i "s|jswindenfahrer.js|$versioned_js|g" sftp_tmp.batch

(cd $dist_dir;zip -9 -r $dist_zip *)

echo scp $dist_dir/$dist_zip p8128049@home33313511.1and1-data.host:steilspirale/schleppkalender

echo "sftp ssh-600202-ssh@dcb.org < sftp_tmp.batch"
